﻿/// Author: Francesco Belvedere
///<summary>
///This small amount of code is to sort a list of patchable routers. 
///Code snippet to check for duplicates in an array "list2.Count(x => x == list2[i]) > 1"
///was researched on StackOverflow for simplicity.
///<summary>
///Date last modified: 23/01/2017


using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatchChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            //Checks whether input has been entered correctly
            CheckInput(args);

            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(args[0]))
                {
                    List<string> list1 = new List<string>();
                    List<string> list2 = new List<string>();
                    List<string> list3 = new List<string>();
                    List<string> list4 = new List<string>();
                    List<string> list5 = new List<string>();


                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] values = line.Split(',');

                        list1.Add(values[0]);
                        list2.Add(values[1]);
                        list3.Add(values[2]);
                        list4.Add(values[3]);
                        list5.Add(values[4]);
                    }

                    for (int i = 0; i < list1.Count(); i++)
                    {
                        // There are no other routers which share the same hostname
                        bool isRepeated = list1.Count(x => x.ToLower() == list1[i].ToLower()) > 1;
                        if (isRepeated)
                            continue;

                        // There are no other routers which share the same IP address
                        isRepeated = list2.Count(x => x == list2[i]) > 1;
                        if (isRepeated)
                            continue;

                        // Check whether the router has not already been patched
                        if (list3[i].ToLower().Equals("yes"))
                            continue;

                        // Check whether the current version of the router OS is 12 or above
                        double tmp;
                        double.TryParse(list4[i], out tmp);
                        if (tmp < 12)
                            continue;


                        //if it has passed all the tests, it can be patched
                        string line = list1[i] + " (" + list2[i] + ") "
                            + "OS Version " + list4[i];

                        if (list5[i].Length > 0)
                            line += " [" + list5[i] + "]";

                        Console.WriteLine(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }



        private static void CheckInput(string [] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("*Please enter the text file location as a " +
                    "command-line argument*");
                Console.ReadKey();
                return;
            }
        }
    }
}
